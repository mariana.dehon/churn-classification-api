import json
from http import HTTPStatus
from unittest import TestCase, main

from jsonschema import validate

from app.main import app

chrun_api = app.test_client()


def __load_json_schema(path):
    with open(path) as schema_file:
        return json.loads(schema_file.read())


def assert_valid_schema(data, path):
    schema = __load_json_schema(path)
    return validate(data, schema)


class EvaluateTest(TestCase):
    def test(self):
        request_json = {'Name': 'Thomas Sawyer',
                        'Age': 48.0,
                        'Total_Purchase': 10367.08,
                        'Account_Manager': 1,
                        'Years': 6.45,
                        'Num_Sites': 12.0,
                        'Onboard_date': '2008-10-06 17:15:00',
                        'Location': '8113 Rodriguez Court Sheribury, SC 88474',
                        'Company': 'Lopez, Kelly and Armstrong'
                        }

        response = chrun_api.post('/predict', json=request_json)

        assert_valid_schema(response.json, 'tests/predict.json')
        self.assertEqual(response.status_code, HTTPStatus.OK)


class MalformedRequestTest(TestCase):
    def test(self):
        request_json = {'Name': 'Alan Turing',
                        'Age': -1,
                        'Total_Purchase': 'None',
                        'Account_Manager': 'not managed',
                        'Years': -4,
                        'Num_Sites': 15,
                        'Location': 'earth',
                        'Company': 'math'
                        }

        response = chrun_api.post('/predict', json=request_json)

        expected_json = {'success': False, 'message': 'Malformed request', 'error_code': 'VALUE_ERROR',
                         'errors': {'Total_Purchase': ['Not a valid float value', 'Number must be at least 0.'],
                                    'Account_Manager': ['Not a valid integer value',
                                                        'Invalid value, must be one of: 0, 1.'],
                                    'Years': ['Number must be at least 0.'],
                                    'Onboard_date': ['This field is required.']}}

        self.assertDictEqual(response.json, expected_json)
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)


class NotFoundRequestTest(TestCase):
    def test(self):
        request_json = {'Name': 'Alan Turing'}

        response = chrun_api.post('/classify', json=request_json)

        expected_json = {'success': False,
                         'message': 'The requested URL was not found on the server. '
                                    'If you entered the URL manually please check your spelling and try again.',
                         'error_code': 'NOT_FOUND'}

        self.assertDictEqual(response.json, expected_json)
        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)


class NotAllowedRequestTest(TestCase):
    def test(self):
        request_json = {'Name': 'Thomas Sawyer',
                        'Age': 48.0,
                        'Total_Purchase': 10367.08,
                        'Account_Manager': 1,
                        'Years': 6.45,
                        'Num_Sites': 12.0,
                        'Onboard_date': '2008-10-06 17:15:00',
                        'Location': '8113 Rodriguez Court Sheribury, SC 88474',
                        'Company': 'Lopez, Kelly and Armstrong'
                        }

        response = chrun_api.get('/predict', json=request_json)

        expected_json = {'success': False,
                         'message': 'The method is not allowed for the requested URL.',
                         'error_code': 'METHOD_NOT_ALLOWED'}

        self.assertDictEqual(response.json, expected_json)
        self.assertEqual(response.status_code, HTTPStatus.METHOD_NOT_ALLOWED)


if __name__ == '__main__':
    main()

from http import HTTPStatus

from flask import Flask
from wtforms.validators import ValidationError

from app import endpoints
from app.exceptions import (ApiException, api_exception_handler, form_exception_handler,
                            flask_not_found_exception_handler, flask_not_allowed_exception_handler,
                            internal_exception_handler)


class FlaskApp(Flask):
    def __init__(self, *args, **kwargs):
        super(FlaskApp, self).__init__(*args, **kwargs)

        self.config['JSON_SORT_KEYS'] = False
        self.config['WTF_CSRF_ENABLED'] = False

        # Exception Handlers
        self.register_error_handler(ApiException, api_exception_handler)
        self.register_error_handler(ValidationError, form_exception_handler)
        self.register_error_handler(HTTPStatus.NOT_FOUND, flask_not_found_exception_handler)
        self.register_error_handler(HTTPStatus.METHOD_NOT_ALLOWED, flask_not_allowed_exception_handler)
        self.register_error_handler(Exception, internal_exception_handler)

        # Endpoints
        self.__add_endpoint('/predict', endpoints.predict, ['POST'])

    def __add_endpoint(self, path, fn, methods):
        self.add_url_rule(path, view_func=fn, methods=methods)


app = FlaskApp(__name__)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)

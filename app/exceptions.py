from collections import OrderedDict
from enum import Enum
from http import HTTPStatus

from flask import jsonify


class ErrorCode(Enum):
    NOT_FOUND = 'NOT_FOUND'
    VALUE_ERROR = 'VALUE_ERROR'
    METHOD_NOT_ALLOWED = 'METHOD_NOT_ALLOWED'
    INTERNAL_SERVER_ERROR = 'INTERNAL_SERVER_ERROR'

    def __str__(self):
        return self.value


class ApiException(Exception):
    def __init__(self, error_code, message, http_status, errors=None):
        super().__init__(message)
        self.__error_code = error_code
        self.__http_status = http_status
        self.__errors = {} if errors is None else errors

    @property
    def error_code(self):
        return self.__error_code

    @property
    def http_status(self):
        return self.__http_status

    @property
    def errors(self):
        return self.__errors


class InvalidFormException(ApiException):
    def __init__(self, message, errors):
        super().__init__(error_code=ErrorCode.VALUE_ERROR, message=message, http_status=HTTPStatus.BAD_REQUEST,
                         errors=errors)


def api_exception_handler(api_exception):
    response_data = OrderedDict({
        'success': False,
        'message': str(api_exception),
        'error_code': str(api_exception.error_code)
    })
    if len(api_exception.errors) > 0:
        response_data['errors'] = api_exception.errors
    return jsonify(response_data), api_exception.http_status


def form_exception_handler(form_exception):
    response_data = OrderedDict({
        'success': False,
        'message': str(form_exception),
        'error_code': str(ErrorCode.VALUE_ERROR)
    })
    return jsonify(response_data), HTTPStatus.BAD_REQUEST


def flask_not_found_exception_handler(not_found_exception):
    response_data = OrderedDict({
        'success': False,
        'message': not_found_exception.description,
        'error_code': str(ErrorCode.NOT_FOUND)
    })
    return jsonify(response_data), HTTPStatus.NOT_FOUND


def flask_not_allowed_exception_handler(not_found_exception):
    response_data = OrderedDict({
        'success': False,
        'message': not_found_exception.description,
        'error_code': str(ErrorCode.METHOD_NOT_ALLOWED)
    })
    return jsonify(response_data), HTTPStatus.METHOD_NOT_ALLOWED


def internal_exception_handler(internal_exception):
    response_data = OrderedDict({
        'success': False,
        'message': 'Internal Server Error',
        'error_code': str(ErrorCode.INTERNAL_SERVER_ERROR)
    })
    # TODO: log exception
    print(internal_exception)
    return jsonify(response_data), HTTPStatus.INTERNAL_SERVER_ERROR

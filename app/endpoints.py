from http import HTTPStatus

from flask import request, jsonify, abort
from flask_wtf import Form
from wtforms import StringField, FloatField, IntegerField, DateTimeField
from wtforms.validators import AnyOf, InputRequired, NumberRange
import wtforms_json

import model.churn_classification_model as model
from .exceptions import InvalidFormException

wtforms_json.init()


class ValidForm(Form):

    def __init__(self, *args, **kwargs):
        super(ValidForm, self).__init__(*args, **kwargs)
        if not self.validate():
            raise InvalidFormException(message='Malformed request', errors=self.errors)


class PredictionForm(ValidForm):
    Name = StringField(label='Name')
    Age = FloatField(label='Age', validators=[InputRequired()])
    Total_Purchase = FloatField(label='Total_Purchase', validators=[InputRequired(), NumberRange(min=0)])
    Account_Manager = IntegerField(label='Account_Manager', validators=[InputRequired(), AnyOf(values=[0, 1])])
    Years = FloatField(label='Years', validators=[InputRequired(), NumberRange(min=0)])
    Num_Sites = FloatField(label='Num_Sites', validators=[InputRequired(), NumberRange(min=0)])
    Onboard_date = DateTimeField(label='Onboard_date', validators=[InputRequired()])
    Location = StringField(label='Location')
    Company = StringField(label='Company')


def predict():
    if request.method == 'POST':
        form = PredictionForm.from_json(request.json)
        churn = model.model_churn.evaluate(form.data)
        response_data = {'churn': churn,
                         'description': 'Positive churn' if churn else 'Negative churn'}
        return jsonify(response_data), HTTPStatus.OK
    return abort(HTTPStatus.NOT_FOUND)

FROM python:3.7-slim-stretch

ARG COMMIT
ENV UWSGI_PROCESSES_PER_CORE 4

ADD requirements.txt /tmp/requirements.txt

RUN set -ex \
    && buildDeps=' \
        gcc \
        libc6-dev \
    ' \
    && apt-get update \
    && apt-get install -y $buildDeps --no-install-recommends \
    && rm -rf /var/lib/apt/lists* \
    && pip install --upgrade pip -i https://pypi.python.org/simple \
    && pip install --no-cache-dir -r /tmp/requirements.txt \
    && apt-get purge -y --auto-remove $buildDeps

RUN mkdir -p /app
RUN mkdir -p /model
RUN mkdir -p /tests

COPY ./ /app
COPY ./run.sh /app
COPY ./ /model
COPY ./ /tests

WORKDIR /app/

EXPOSE 5000

CMD bash run.sh
#!/bin/bash
set -e

echo "INFO: Running on commit ${COMMIT}"

# starts app
n_processes=$((`nproc` * ${UWSGI_PROCESSES_PER_CORE}))
echo "INFO: Starting ${n_processes} uwsgi workers..."
uwsgi --wsgi-file ./app/main_uwsgi.py \
      --http :5000 \
      --processes ${n_processes} \
      --enable-threads \
      --master \
      --disable-logging
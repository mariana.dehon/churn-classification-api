import warnings

warnings.simplefilter(action='ignore', category=FutureWarning)

import os
import pickle
import pandas as pd

from datetime import datetime

from sklearn.model_selection import train_test_split, KFold, cross_val_score
from imblearn.over_sampling import SMOTE
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import make_column_transformer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC, NuSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier
from sklearn.metrics import confusion_matrix, auc, roc_curve
from sklearn.naive_bayes import GaussianNB
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
import matplotlib.pyplot as plt

MODEL_PATH = os.getenv('MODEL_PATH', './model/model.obj')
CSV_PATH = os.getenv('CSV_PATH', './model/dataset_churn.csv')
DEBUG_MODE = True if os.getenv('DEBUG_MODE') else False


class Model:

    def __init__(self, debug_mode=False, test_size=0.2):
        self.__best_classifier = None
        self.__debug_mode = debug_mode
        self.__test_size = test_size

    def init_core_model(self):
        with open(MODEL_PATH, 'rb') as fp:
            self.__best_classifier = pickle.load(fp)

    def dump_core_model(self):
        with open(MODEL_PATH, 'wb') as fp:
            pickle.dump(self.__best_classifier, fp)

    @staticmethod
    def __diff_from_current_time(date):
        currentDT = datetime.now()
        date = datetime.fromisoformat(date) if isinstance(date, str) else date
        result = currentDT - date
        return result.days

    def evaluate(self, dict_input):
        df = pd.DataFrame(dict_input, index=[0])
        drop_variables = ['Name', 'Location', 'Company']
        df = df.drop(drop_variables, axis=1)
        df.Onboard_date = df.Onboard_date.apply(lambda x: Model.__diff_from_current_time(x))
        return int(self.__best_classifier.predict(df))

    def start_training(self, csv_path):
        df, preprocessor = Model.__preprocessing(csv_path)
        X_train, X_test, y_train, y_test = self.__divide_dataframe(df)
        self.__best_classifier = self.__find_best_classifier(preprocessor, X_train, X_test, y_train, y_test)

    @staticmethod
    def __preprocessing(csv_path):
        df = pd.read_csv(csv_path)
        # Remove label variables
        drop_variables = ['Names', 'Location', 'Company']
        df = df.drop(drop_variables, axis=1)

        # Drop missing values
        df = df.dropna()

        # Transform Onboard_date variable
        df.Onboard_date = df.Onboard_date.apply(lambda x: Model.__diff_from_current_time(x))

        # Scale numeric values for non categorical variables
        numeric_features = df.drop(labels=['Account_Manager', 'Churn', 'Onboard_date'], axis=1).columns

        preprocess = make_column_transformer((numeric_features, StandardScaler()))

        return df, preprocess

    def __divide_dataframe(self, df):
        # Get test and training sets
        X = df.drop('Churn', axis=1)
        y = df['Churn']

        # Divide dataset
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=self.__test_size)

        # Balance training classes if necessary
        smote = SMOTE(ratio='minority')
        X_resampled, y_resampled = smote.fit_sample(X_train, y_train)
        X_resampled = pd.DataFrame(X_resampled, columns=X_train.columns)

        return X_resampled, X_test, y_resampled, y_test

    def __find_best_classifier(self, preprocessor, X_train, X_test, y_train, y_test):
        classifiers = {
            'KNC': KNeighborsClassifier(3),
            'SVC': SVC(kernel="rbf", C=0.025, probability=True),
            'NuSVC': NuSVC(probability=True),
            'DTC': DecisionTreeClassifier(),
            'ABC': AdaBoostClassifier(),
            'GBC': GradientBoostingClassifier(),
            'Gaussian': GaussianProcessClassifier(),
            'GNB': GaussianNB(),
            'QDA': QuadraticDiscriminantAnalysis()
        }

        results = []
        best_model = None
        best_accuracy = -1

        if self.__debug_mode:
            msg = '\nTraining evaluation\n{}: {} ({})\n'.format('Model', 'mean', 'std')
            print(msg)

        for name, classifier in classifiers.items():
            pipe = make_pipeline(preprocessor, classifier)
            kfold = KFold(n_splits=10)
            cv_results = cross_val_score(pipe, X_train, y_train, cv=kfold, scoring='accuracy')
            results.append(cv_results)

            if self.__debug_mode:
                msg = '{}: {} ({})'.format(name, cv_results.mean(), cv_results.std())
                print(msg)

            if cv_results.mean() > best_accuracy:
                best_accuracy = cv_results.mean()
                best_model = name

        if self.__debug_mode:
            fig, ax = plt.subplots()
            ax.set_title('Algorithm Comparison - Train')
            ax.boxplot(results)
            ax.set_xticklabels(classifiers.keys())
            plt.savefig('boxplot_comparison.pdf')
            plt.close(fig)

        pipe_best_model = make_pipeline(preprocessor, classifiers[best_model])
        pipe_best_model.fit(X_train, y_train)

        if self.__debug_mode:
            print('\nBest Model: {}'.format(best_model))
            print("model score (Test): %.3f" % pipe_best_model.score(X_test, y_test))

            y_pred = pipe_best_model.predict(X_test)
            cm = confusion_matrix(y_test, y_pred)
            print('Confusion matrix: \n')
            print(cm)

            y_pred_sample_score = pipe_best_model.decision_function(X_test)

            fpr, tpr, thresholds = roc_curve(y_test, y_pred_sample_score)

            roc_auc = auc(fpr, tpr)

            # Plot ROC
            plt.title('Receiver Operating Characteristic')
            plt.plot(fpr, tpr, 'b', label='AUC = %0.3f' % roc_auc)
            plt.legend(loc='lower right')
            plt.plot([0, 1], [0, 1], 'r--')
            plt.xlim([-0.1, 1.0])
            plt.ylim([-0.1, 1.01])
            plt.ylabel('True Positive Rate')
            plt.xlabel('False Positive Rate')
            plt.savefig('roc_curve_bestmode.pdf')

            print("\nCheck output graphs on model's directory")

        return pipe_best_model


model_churn = Model(debug_mode=DEBUG_MODE)

if not DEBUG_MODE and os.path.exists(MODEL_PATH):
    model_churn.init_core_model()
else:
    model_churn.start_training(CSV_PATH)
    model_churn.dump_core_model()

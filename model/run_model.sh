#!/bin/bash

set -e

# start training
echo "INFO: Starting training..."
python3.7 churn_classification_model.py

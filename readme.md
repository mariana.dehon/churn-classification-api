# Churn Classification API

![](https://gitlab.com/mariana.dehon/churn-classification-api/badges/master/pipeline.svg)

## Sections
* [Execution Instructions](#execution-instructions)  
* [API - Approach](#api-rest)
* [Machine Learning - Approach](#machine-learning-model)

---

##### Execution Instructions

- Prerequisites:

[Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and
[Docker](https://docs.docker.com)

- Clone repository

``` bash
git clone https://gitlab.com/mariana.dehon/churn-classification-api.git
```

- Run Churn API REST (background)

``` bash
cd churn-classification-api
docker build --no-cache --rm -t churn_classification_api:dev -f Dockerfile .
docker run --rm -d --network host --cpus=$(nproc) --name churn_classification_api churn_classification_api:dev
```

- Stop API REST

``` bash
docker kill churn_classification_api
```

- Run Machine Learning training (optional)


``` bash
cd churn-classification-api/model
docker build --no-cache --rm -t churn_classification_model:dev -f Dockerfile.model .
docker run --rm -it --env CSV_PATH=dataset_churn.csv -v <LOCAL_PATH>/churn-classification-api/model:/model/  churn_classification_model:dev
```


##### Requisitions

The Churn Classification API accepts POST requisitions to predict if a costumer will churn or not.

* **_curl example:_**

```bash
curl --verbose -H "Content-Type: application/json" -d '{"Name": "Thomas Sawyer", "Age": 48.0, "Total_Purchase": 10367.08, "Account_Manager": 1, "Years": 6.45, "Num_Sites": 12.0, "Onboard_date": "2008-10-06 17:15:00", "Location": "8113 Rodriguez Court Sheribury, SC 88474","Company": "Lopez, Kelly and Armstrong"}' http://localhost:5000/predict
```

```
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 5000 (#0)
> POST /predict HTTP/1.1
> Host: localhost:5000
> User-Agent: curl/7.58.0
> Accept: */*
> Content-Type: application/json
> Content-Length: 257
> 
* upload completely sent off: 257 out of 257 bytes
< HTTP/1.1 200 OK
< Content-Type: application/json
< Content-Length: 43
< 
{"churn":1,"description":"Positive churn"}
* Connection #0 to host localhost left intact
```

* **_Python Example:_**

```python
import requests

url = "http://localhost:5000/predict"
request_data = {'Name': 'Thomas Sawyer',
                'Age': 48.0,
                'Total_Purchase': 10367.08,
                'Account_Manager': 1,
                'Years': 6.45,
                'Num_Sites': 12.0,
                'Onboard_date': '2008-10-06 17:15:00',
                'Location': '8113 Rodriguez Court Sheribury, SC 88474',
                'Company': 'Lopez, Kelly and Armstrong'
                }

response = requests.request("POST", url, json=request_data)
result = 'HTTPStatusCode: {}\nHeader: {}\nJSON: {}'.format(response.status_code, response.headers, response.json())
print(result)
```

```
HTTPStatus: 200
Header: {'Content-Type': 'application/json', 'Content-Length': '43'}
JSON: {'churn': 1, 'description': 'Positive churn'}
```

### Approach

**Programming Language:** [Python 3.7](https://www.python.org/downloads/release/python-370/)

#### API Rest
  * **Reproducibility:** used [Docker](https://www.docker.com/);
  * **Code and quality analysis:** code style check with [pycodestyle](https://www.mankier.com/1/pycodestyle), build test with Docker in Docker ([dind](https://hub.docker.com/_/docker)) service, automated tests using [unittest](https://docs.python.org/3/library/unittest.html) and test covarage with [Coverage.py](https://coverage.readthedocs.io/en/v4.5.x/); 
  * **Continuous Integration/Delivery:** test pipeline with [Gitlab-CI](https://about.gitlab.com/product/continuous-integration/);
  * **Scalability:** used [uWSGI](https://uwsgi-docs.readthedocs.io/en/latest/);
  * **Error handling:** [Flask Exception Handler](https://flask.palletsprojects.com/en/1.0.x/errorhandling/);
  * **Form Validation:** [WTForms](https://flask.palletsprojects.com/en/1.0.x/patterns/wtforms/).

---

#### Machine Learning Model
**Pipeline approach**: [sklearn.pipeline](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html)
  
**Data transformation sequence (pre-processing)**: 
  1. Remove label variables (`Names`,`Location`, `Company`);
  1. Drop missing values; 
  1. Transform `Onboard_date` in a numeric value (difference in days to the current date);
  1. Scale non categorical variables using [StandardScaler](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html).
  
**Data division**:
  1. Train and test division using  [`train_test_split`](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.train_test_split.html) with a test ratio of `0.2`;
  1. Balance training data with [`SMOTE`](https://imbalanced-learn.readthedocs.io/en/stable/generated/imblearn.over_sampling.SMOTE.html) using the configuration `ratio='minority'`.
  
**Find best model**:
  1. Tested the following classifiers from [scikit-learn](https://scikit-learn.org/stable/) library: `KNeighborsClassifier`, `SVC`, `NuSVC`, `DecisionTreeClassifier`, `AdaBoostClassifier`, `GradientBoostingClassifier`, `GaussianProcessClassifier`, `GaussianNB` and `QuadraticDiscriminantAnalysis`;
  2. For simplicity's sake, the classifiers were tested using default settings;
  3. Cross-validation using [KFold](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.KFold.html) with `10` folds and scoring based on `accuracy`;
  4. In this approach, the best model is the one with the higher `accuracy` on training set;
  5. The best model is saved on a [pickle](https://docs.python.org/3/library/pickle.html) object.
